import React from 'react'
import Navigation from './components/navigation';
import Header from './components/headers';
import Charts from './components/charts';
import Credits from './components/credits';
import Advices from './components/advices';
import { DashboardProvider } from './pages/DashboardContext';
import Sidebar from './components/Sidebar';
import "./style/scss/style.scss"
import "./style/scss/navbar.scss"
import "./style/scss/headers.scss"
import "./style/scss/chart.scss"
import "./style/scss/credit.scss"
import "./style/scss/contact.scss"
import "./style/scss/sidebar.scss"

const App =()=> {
    return (
      <div>
          <DashboardProvider>
            <Navigation />
            <Sidebar/>
            <div className="content" id="main">
              <Header />
              <Charts />
              <Credits />
              <Advices />
            </div>
          </DashboardProvider>
      </div>
    )
}

export default App;
