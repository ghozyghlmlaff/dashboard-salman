import React, { useContext } from 'react'
import { DashboardContext } from "../pages/DashboardContext";

export const Sidebar = () => {

    const [,,,,,,sidebar] = useContext(DashboardContext)
    let animationClasses = (sidebar ? ' active': '');    
    return (
        <>
            <div className={`sidebar${animationClasses}`} id="sidebar">
                <ul className="nav">
                    <li className="nav-item nav-profile">
                    <div className="nav-link">
                        <div className="profile-image">
                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Circle-icons-profile.svg/1200px-Circle-icons-profile.svg.png" className="img-xs rounded-circle" alt="profile" />
                            <div className="dot-indicator bg-success" />
                        </div>
                        <div className="text-wrapper">
                            <h5 className="profile-name">Ghozy Ghulamul Afif</h5>
                            <h5 className="designation">Role : Admin</h5>
                        </div>
                    </div>
                    </li>
                    <li className="nav-item nav-category">Main Menu</li>
                    <li className="nav-item">
                    {/* <Link className="nav-link" to="/">
                        <i className="menu-icon typcn typcn-document-text" />
                        <span className="menu-title">Users</span>
                    </Link> */}
                    <a className="sidebar-link" href="#charts">Charts</a>
                    </li>
                    <li className="nav-item">
                    {/* <Link className="nav-link" to="/approval">
                        <i className="menu-icon typcn typcn-document-text" />
                        <span className="menu-title">Approval Users</span>
                    </Link> */}
                    <a className="sidebar-link" href="#credits">Credits</a>
                    </li>
                    <li className="nav-item">
                    {/* <Link className="nav-link" to="/pegawai">
                        <i className="menu-icon typcn typcn-document-text" />
                        <span className="menu-title">List Pegawai</span>
                    </Link> */}
                    <a className="sidebar-link" href="#advice">Advices</a>
                    </li>
                </ul>
                {/* <a className="sidebar-link" href="#charts">Charts</a>
                <a className="sidebar-link" href="#credits">Credits</a>
                <a className="sidebar-link" href="#advice">Advices</a> */}
            </div>
        </>
    );
}
export default Sidebar;