import React, {useContext, useState} from "react";
import { DashboardContext } from "../pages/DashboardContext";

const Navigation = () => {
    const scrollToTop = ()=> {
      window.scrollTo({
        top: 0,
        behavior: "smooth"
      });
    }

    const [,,,,,,sidebar, setSidebar] = useContext(DashboardContext)
    const showSidebar = () => {
      setSidebar(!sidebar)
    } 

    const [navbar, setNavbar] = useState(false)
    const showDropdown = () => {
      setNavbar(!navbar)
    } 

    let animationClasses = (navbar ? ' active': '');    

    return (
      <nav id="menu" className="navbar navbar-default navbar-fixed-top">
        <div className="container">
          <div className="navbar-header">
            <button
              type="button"
              className="navbar-toggle collapsed"
              data-toggle="collapse"
              onClick={() => showSidebar()}
            >
              <span className="icon-bar"><i className="fa fa-bars" aria-hidden="true"></i></span>
            </button>
            <div className="navbar-brand" onClick={() => scrollToTop()}>
              Covidopedia
            </div>
            <div className="navbar-dropdown" onClick={() => showDropdown()}>
                <ul className={`navbar-nav${animationClasses}`}>
                    <li className="nav-item dropdown user-dropdown">
                        <div className="dropdown-menu dropdown-menu-right" aria-labelledby="UserDropdown">
                            <div className="dropdown-header text-center">
                                <img className="img-md rounded-circle" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Circle-icons-profile.svg/1200px-Circle-icons-profile.svg.png" alt="Profile" />
                                <p className="name-dropdown">Ghozy Ghulamul Afif</p>
                                <p className="role-dropdown">Admin</p>
                            </div>
                            <button className="dropdown-item">Sign Out</button>
                        </div>
                    </li>
                </ul>
                <img className="img-xs" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Circle-icons-profile.svg/1200px-Circle-icons-profile.svg.png" alt="Profile" width={35}  height={35}/>
            </div>
          </div>
        </div>
      </nav>
    );
}

export default Navigation;
