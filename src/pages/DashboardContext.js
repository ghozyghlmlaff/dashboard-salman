import React, { useState, createContext } from "react";

export const DashboardContext = createContext();

export const DashboardProvider = props => {
    const [covid, setCovid] =  useState([])
    const [covidBar, setCovidBar] =  useState([])
    const [sidebar, setSidebar] = useState(false); 
    const [detailCovid, setDetail] = useState({cases:0, deaths:0, recovered:0, last_update:new Date()})

    return (
        <DashboardContext.Provider value={[covid, setCovid, covidBar, setCovidBar, 
                                            detailCovid, setDetail, sidebar, setSidebar]}>
            {props.children}
        </DashboardContext.Provider>
    );
};